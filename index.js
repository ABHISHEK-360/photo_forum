var express = require('express');
var passport = require('passport');
var session = require('express-session');
var bodyParser = require('body-parser');
var models = require("./src/models");
var env = require('dotenv');
var routes = require("./src/controllers");
var path = require('path');
var cors = require('cors');

var app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());

app.use('/api',routes);

app.use(express.static(path.join(__dirname, 'client/build')));


// app.use(session({ secret: 'abhishek360 development', resave: true, saveUninitialized: true }));
//
// app.use(passport.initialize());
// app.use(passport.session());

models.sequelize.sync().then(function(){
  console.log('Nice! Databse connected.')
}).catch(function(err) {
  console.log(err);
})

app.get('/*',function(req,res){
    res.sendFile(path.join(__dirname+'/client/build/index.html'));
});

app.listen(process.env.PORT || 4000, function(err){
  if(!err){
    console.log('A360 listening on port', process.env.PORT || 4000);
  }
  else{
    console.log(err);
  }
})
