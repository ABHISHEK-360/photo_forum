//abhishek360

var bcrypt = require('bcrypt');
var express = require('express');
var router = express.Router();
var { User } = require('../models');

router.post('/signup', async(req, res) => {

  const hash = bcrypt.hashSync(req.body.password, 10);

  try {
    let user = await User.create(
      Object.assign({ username: req.body.email, createdAt: new Date() }, { password: hash })
    );
    //console.log('body--------',req.body);

    //let data = await user.authorize();
    let details = await user.addDetails(req.body);

    return res.json(
      {
        success: true,
        message: 'User registered successfully.'
      }
    );
  }
  catch(err){
    console.log('signup err--------',err);
    return res.status(400).send(err);
  }
});

router.post('/login', async (req, res) => {
  const { username, password } = req.body;

  if(!username||!password){
    // return res.status(400).send({
    //   success: false,
    //   message: 'Request missing login details param',
    // });
    return res.status(400).send(
      'missing login details params'
    );
  }

  try {
    let user = await User.authenticate(username, password);

    //user = await user.authorize();
    return res.json(
      {
        success: true,
        id: user.user.id,
        verification: user.user.verification,
        username: user.user.username,
        createdAt: user.authToken.createdAt,
        token: user.authToken.token
      }
    );

  }
  catch(err) {
    // return res.status(400).send({
    //   success: false,
    //   message: 'invalid username or password'
    // });
    return res.status(400).send(
      'invalid username or password'
    );
  }
})

router.put('/logout', async(req, res) => {
  const authToken = req.header('xyz-access-token');

  if(authToken) {
    const result = await User.logout(authToken);
    return res.send(
      {
        success: result,
        message: 'user logged out successfully!'
      }
    );
  }

  return res.status(400).send(
    {
      success: false,
      message: 'not authenticated'
    }
  );
})

router.get('/details', async ( req, res ) => {
  const authToken = req.header('xyz-access-token');

  if(authToken){
    try {
      const userDetails = await User.getDetails(authToken);
      return res.send(userDetails);
    } catch (e) {
      return res.status(404).send(
        {
          success: false,
          message: 'invalid auth token or session expired',
        }
      );
    }

  }
  return res.status(404).send(
    {
      success: false,
      message: 'missing auth token'
    }
  );
})

router.post('/update', async ( req, res ) => {
  const authToken = req.header('xyz-access-token');
  const { lastName, firstName } = req.body;

  if(authToken){
    try {
      const userDetails = await User.updateDetails(authToken, lastName, firstName);
      return res.send(userDetails);
    } catch (e) {
      return res.status(404).send(
        {
          success: false,
          message: 'invalid auth token'
        }
      );
    }
    //return res.send(userDetails);
  }
  return res.status(404).send(
    {
      success: false,
      message: 'missing auth token'
    }
  );
})

module.exports = router;
