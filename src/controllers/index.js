var express = require('express');
var router = express.Router();
var userController = require('./userController');

router.use('/users', userController);
// router.use('/api/users', (req,res) => {
//   console.log('request at /api/users on', new Date());
//   return new userController(req, res);
// });

module.exports = router;
