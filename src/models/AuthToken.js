module.exports = (sequelize, DataTypes) => {
  const AuthToken = sequelize.define('AuthToken', {
    id: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false
    },
    token: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    createdAt: {
      type: DataTypes.DATE,
    }
  }, {});

  AuthToken.associate = function(models) {
    AuthToken.belongsTo(models.User);
  };

  AuthToken.generate = async function(UserId) {
    if(!UserId){
      throw new Error('AuthToken requires a user ID')
    }

    let token = '';

    const possibleCharacters = 'QWERTYUIOPASDFGHJKLMNBVCXZ'+
      'zxcvbnmlkjhgfdsaqwertyuiop'+'0123456789';

    for(var i =0; i<21; i++){
      token += possibleCharacters.charAt(
        Math.floor(Math.random() * possibleCharacters.length)
      );
    }

    return AuthToken.create({ token, UserId, createdAt: new Date() })
  }

  return AuthToken;
};
