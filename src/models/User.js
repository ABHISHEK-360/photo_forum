//abhishek360
var bcrypt = require('bcrypt');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User',{
    id: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false
    },
    username: {
      type: DataTypes.STRING,
      unique: true,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    lastLogin: {
      type: DataTypes.DATE,
    },
    createdAt: {
      type: DataTypes.DATE,
    },
    verification: {
      type: DataTypes.ENUM('verified', 'pending'),
      defaultValue: 'pending',
    },
    status: {
      type: DataTypes.ENUM('active', 'inactive'),
      defaultValue: 'active',
    }
  });

  User.associate = function(models) {
    User.hasMany(models.AuthToken);
    User.hasOne(models.UserDetails);
  }

  User.authenticate = async function( username, password ){
    const user = await User.findOne({ where: { username }});

    if(bcrypt.compareSync( password, user.password )) {
      user.update(
        {
          lastLogin: new Date()
        }
      );
      return user.authorize();
    }

    throw new Error('invalid password');
  }

  User.isTokenExpired = function(authToken) {

    const curr = new Date();
    const expiry = 30*60*1000;
    if(curr-authToken.createdAt>expiry){
      authToken.destroy();
      return true;
    }
    else return false;
  }

  User.getDetails = async function(token) {
    const { AuthToken, UserDetails } = sequelize.models;
    const authToken = await AuthToken.findOne({ where: { token }});
    if(User.isTokenExpired(authToken)){
      throw new Error('Session Expired!');
    };
    const userDetails = await UserDetails.findOne({ where: { UserId: authToken.UserId }});
    //console.log('userDetails-----------', userDetails);
    return {
      success: true,
      ...userDetails.dataValues,
    }
  }

  User.updateDetails = async function(token, lastName, firstName) {
    const { AuthToken, UserDetails } = sequelize.models;
    const authToken = await AuthToken.findOne({ where: { token }});
    //console.log('auth token',authToken.UserId);

    var userDetails = await UserDetails.findOne(
        { where: { UserId: authToken.UserId }}
      );

  //console.log('userDetails',userDetails);


    //return userDetails;

    await userDetails.update(
      {
        lastName,
        firstName
      }
    );

    return {
      success: true,
      ...userDetails.dataValues,
    }
  }

  User.prototype.addDetails = async function( details ) {
    const { UserDetails } = sequelize.models;
    const updatedAt = new Date();
    const userId = this.id;


    const userDetails = await UserDetails.create(
      Object.assign(details, { updatedAt, UserId: userId })
    );

    //await user.setUserDetails(userDetails);

    return userDetails;
  }

  User.prototype.authorize = async function() {
    const { AuthToken } = sequelize.models;
    const user = this;

    const authToken = await AuthToken.generate(this.id);

    await user.addAuthToken(authToken);

    return {
      user,
      authToken
    };
  }

  User.logout = async function(token) {
    sequelize.models.AuthToken.destroy({ where: { token }});
    return true;
  }

  return User;
};
