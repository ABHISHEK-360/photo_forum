//abhishek360

module.exports = (sequelize, DataTypes) => {
  const UserDetails = sequelize.define('UserDetails',{
    id: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    lastName: {
      type: DataTypes.STRING,
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    updatedAt: {
      type: DataTypes.DATE,
    },
  });

  UserDetails.associate = function(models) {
    UserDetails.belongsTo(models.User);
  }

  return UserDetails;
};
