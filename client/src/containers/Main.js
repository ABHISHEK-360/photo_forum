//abhishek360

import React, { Component } from 'react';
import Home from '../components/Home';
import About from '../components/About';
import Photography from '../components/Photography';
import Arts from '../components/Arts';
import Architecture from '../components/Architecture';
import Header from '../components/Header';
import Footer from '../components/Footer';
import Popup from '../components/Popup';
import RequestService from '../services/RequestService';
import { Router, Link } from '@reach/router';
import { connect } from 'react-redux';
import {
  userLogout,
  fetchUserDetails,
} from '../actions/UserStateActions';

class Main extends Component{
  state = {
    welcome: 'Welcome to Ourforum',
    showPopup: false,
    popupType: 'login'
  }

  constructor(){
    super();
    this.requestService = new RequestService('users','ADMIN')
  }

  async componentDidMount() {
    const loggedIn = await this.requestService.loggedIn();
    if(loggedIn){
      this.props.fetchUserDetails();
    }
  }

  togglePopup = (type) => {
    this.setState({
      popupType: type,
      showPopup: !this.state.showPopup,
    })
  }

  changePopupType = (type) => {
    this.setState({
      popupType: type,
    })
  }

  handleLogin = async ( username, password ) => {
    const data = await this.requestService.auth( username, password );
    console.log('loginnnnnnnnnnnnnnn', data);
    if(data.success){
      //alert('User Logged In!');
      this.props.fetchUserDetails();
      this.togglePopup('');
    }
    else{
      alert('Try Again, Failed to Login!');
    }
  }

  handleRegister = async ( user ) => {
    const data = await this.requestService.reg( user );
    console.log('registeredddddddddddddddddd', data);
    if(data.success){
      alert('User Registered successfully, please login to continue.');
      this.togglePopup('login');
    }
    else{
      alert('Try Again, Failed to register!');
    }
  }

  handleLogout = async () => {
    await this.requestService.logout();
    this.props.userLogout();

  }

  render(){
    return (
      <div style = { styles.container }>
        <Header
          handleLogout = { this.handleLogout }
          togglePopup = { this.togglePopup }
        />
        {
          this.state.showPopup &&
          <Popup
            type = { this.state.popupType }
            closePopup = { this.togglePopup }
            handleLogin = { this.handleLogin }
            handleRegister = { this.handleRegister }
            changePopupType = { this.changePopupType }
          />
        }
        <Router>
          <Home path = '/'/>
          <About path = 'aboutus'/>
          <Photography path = 'photography'/>
          <Architecture path = 'architecture'/>
          <Arts path = 'arts'/>
        </Router >
      </div>
    )
  }
}

const styles = {
  container: {

  },
}

const mapStateToProps = ({ userDetails }) => ({ userDetails });

export default connect(mapStateToProps, {
    userLogout,
    fetchUserDetails,
  })(Main);
