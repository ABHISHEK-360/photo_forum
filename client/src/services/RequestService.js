//abhishek360

import axios from 'axios';
import * as Constants from '../constants/action-constants'

export default class RequestService {
  constructor(route = '', domain = '') {
    let uri = process.env.REACT_APP_ADMIN_HOST || '';
    switch (domain) {
      case Constants.ADMIN:
        uri = process.env.REACT_APP_ADMIN_HOST || '';
        this.url = `${uri}/api/${route}`;
        break;

      default:
        uri = process.env.REACT_APP_ADMIN_HOST || '';
        this.url = `${uri}/api/${route}`;
    }

    axios.defaults.headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    };
  }

  setToken = async(data) => {
    localStorage.setItem('xyz-access-token',data.token);
    localStorage.setItem('createdAt',data.createdAt);
  }

  getToken = async() => {
    const token = await localStorage.getItem('xyz-access-token');
    return token;
  }

  loggedIn = async() => {
    const token = await localStorage.getItem('xyz-access-token');
    if(token!==null&&token.length===21){
      console.log('tokeeeennnnnnnnnnnnnnnn', token);
      return true;
    }
    return false;
  }

  logout = async () => {
    if (await this.loggedIn()) {
      axios.defaults.headers['xyz-access-token'] = await this.getToken();
      try {
        const res = await axios({ method: 'PUT',  url: `${this.url}/logout`});
        await localStorage.clear();
        return res.data;
      } catch (error) {
        return error.response;
      }
    }
    // await localStorage.removeItem("xyz-access-token");
    // await localStorage.removeItem("createdAt");
  }

  post = async (data, headers = axios.defaults.headers, id = '') => {
    if (await this.loggedIn()) {
      axios.defaults.headers['xyz-access-token'] = await this.getToken();
      try {
        const res = await axios({ method: 'POST',  url: `${this.url}${id}`, data, headers });
        return res.data;
      } catch (error) {
        return error.response.data;
      }
    }
    return {
      success: 'false',
      status: 'unauthorized',
      message: 'Token not found, Login and try again.',
    };
  };

  put = async (data, headers = axios.defaults.headers, id = '') => {
    if (await this.loggedIn()) {
      axios.defaults.headers['xyz-access-token'] = await this.getToken();
      try {
        const res = await axios({ method: 'PUT',  url: `${this.url}${id}`, data, headers });
        return res.data;
      } catch (error) {
        return error.response;
      }
    }
    return {
      success: 'false',
      status: 'unauthorized',
      message: 'Token not found, Login and try again.',
    };
  };

  get = async (id = '') => {
    if (await this.loggedIn()) {
      axios.defaults.headers['xyz-access-token'] = await this.getToken();
      try {
        const res = await axios({ methods: 'GET', url: `${this.url}${id}` });
        return res.data;
      } catch (error) {
        return error.response.data;
      }
    }
    else{
      try {
        const res = await axios({ methods: 'GET', url: `${this.url}${id}` });
        return res.data;
      } catch (error) {
        return error.response.data;
      }
    }
  };

  auth = async(username, password) => {
    try {
      const res = await axios({ method: 'POST', url: `${this.url}/login`, data : { username, password } });
      await this.setToken(res.data);
      return res.data;
    } catch (error) {
      console.log('login erorrrrrrrr', error);
      return{
        success: false,
        ...error.response,
      }
    }
  }

  reg = async(user) => {
    try {
      const res = await axios({ method: 'POST', url: `${this.url}/signup`, data : user });
      return res.data;
    } catch (error) {
      return{
        success: false,
        ...error.response,
      }
    }
  }
}
