//abhishek360

//import React, { Component } from 'react';
import { connect }  from 'react-redux';


const AuthHOC =  ( props ) => {
  return props.userDetails.firstName? props.yes() : props.no()
  // return class LoadingHOC extends Component {
  //   state = {
  //     loggedIn: false,
  //   }
  //
  //   componentDidMount() {
  //     if(requestService.loggedIn()){
  //       this.setState({
  //         loggedIn: true,
  //       })
  //     }
  //   }
  //
  //   render () {
  //     return this.state.loggedIn? this.props.yes() : this.props.no()
  //   }
  // }

}

AuthHOC.defaultProps = {
  yes: () => null,
  no: () => null
};

const mapStateToProps = ({ userDetails }) => ({ userDetails });

export default connect(mapStateToProps, {

  })(AuthHOC);
