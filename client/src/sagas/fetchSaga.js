import { all, takeEvery } from 'redux-saga/effects';

import {
  FETCH_USER_DETAILS,
} from '../constants/action-constants';

import { fetchWorkerSaga }  from './getSagasHelpers';

export default function* fetchSagas() {
  try {
    yield all([
      takeEvery(FETCH_USER_DETAILS, fetchWorkerSaga),
    ])
  }
  catch (error) {
    console.log('errors in get sagas ==> ', error);
  }
}
