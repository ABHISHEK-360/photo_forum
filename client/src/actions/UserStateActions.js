import {
  ADMIN,
  FETCH_USER_DETAILS,
  USER_LOGOUT,
} from '../constants/action-constants';

export const fetchUserDetails = ()=>(
  {
    type : FETCH_USER_DETAILS,
    route : `users/details`,
    domain : ADMIN,
  }
)

export const userLogout = ( ) => ({
  type: USER_LOGOUT,
});
