//abhishek360

import React, { Component } from 'react';
import { Router, Link } from "@reach/router";
import{
  List,
  ListItem,
  ListItemText,
} from '@material-ui/core';
import * as Colors from '../configs/colors'


class NavBar extends Component {
  render(){
    return (
      <List component = 'nav'>
        <ListItem component = 'div'>
          <ListItemText inset>
            <Link getProps = {({isCurrent} ) =>{
              return {
                style:{
                  ...styles.linkText,
                  color: isCurrent ? Colors.LIGHT_CRIMSON : Colors.QUICK_SILVER,
                }
              }
            }}

            to="/"
            >
              Home
            </Link>
          </ListItemText>
          <ListItemText inset>
            <Link to="aboutus" getProps = {({isCurrent} ) =>{
              return {
                style:{
                  ...styles.linkText,
                  color: isCurrent ? Colors.LIGHT_CRIMSON : Colors.QUICK_SILVER,
                }
              }
            }}
            >
              About Us
            </Link>
          </ListItemText>
          <ListItemText inset>
            <Link to="photography"
              getProps = {({isCurrent} ) =>{
                return {
                  style:{
                    ...styles.linkText,
                    color: isCurrent ? Colors.LIGHT_CRIMSON : Colors.QUICK_SILVER,
                  }
                }
              }}
            >
              Photography
            </Link>
            </ListItemText>
          <ListItemText inset>
            <Link to="architecture"
              getProps = {({isCurrent} ) =>{
                return {
                  style:{
                    ...styles.linkText,
                    color: isCurrent ? Colors.LIGHT_CRIMSON : Colors.QUICK_SILVER,
                  }
                }
              }}
            >
              Architecture
            </Link>
          </ListItemText>
          <ListItemText inset>
            <Link to="arts"
              getProps = {({isCurrent} ) =>{
                return {
                  style:{
                    ...styles.linkText,
                    color: isCurrent ? Colors.LIGHT_CRIMSON : Colors.QUICK_SILVER,
                  }
                }
              }}
            >
              Arts
            </Link>
          </ListItemText>
        </ListItem>
      </List>
    )
  }
}

const styles = {
  linkText: {
    textDecoration: 'none',
  },
}

export default NavBar;
