//abhishek360

import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Grid,
  Typography,
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
} from '@material-ui/core'
import {
  fetchUserDetails,
} from '../actions/UserStateActions';
import { navigate } from '@reach/router';
import {
  category,
} from '../configs/content';
import * as Colors from '../configs/colors';

class Home extends Component {

  render(){
    return (
      <div style = {styles.container}>
        <Grid
          container
          direction = 'row'
          justify = 'space-evenly'
          alignItems = 'flex-end'
          style = {{ height: '90%'}}
        >
          {
            category.map(item => {
              return (
                <Grid
                  item xs = {2}
                  key = { item.title }
                  onClick = {() => navigate(`${item.value}`)}
                >
                <Card >
                  <CardActionArea style = { styles.actionAreaCard }>
                    <CardMedia
                      component = "img"
                      alt = "asdfghj"
                      style = {{ height: '100%', width: '100%' }}
                      image = { item.img }
                      tittle = 'Photography'
                    />
                    <CardContent>
                      <Typography
                        gutterBottom
                        variant = 'h4'
                        component = 'h2'
                      >
                        { item.title }
                      </Typography>
                      <Typography
                        gutterBottom
                        variant = 'h6'
                        component = 'h2'
                      >
                        { item.desc }
                      </Typography>
                    </CardContent>
                  </CardActionArea>
                </Card>
              </Grid>
              )
            })
          }
        </Grid>
      </div>
    );
  }
}

const styles = {
  container: {
    height: '100%',
    backgroundColor: Colors.AQUAMARINE
  },
  actionAreaCard: {
  },

};

const mapStateToProps = ({ userDetails }) => ({ userDetails });

export default connect(mapStateToProps, {
    fetchUserDetails,
  })(Home);
