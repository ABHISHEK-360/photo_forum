//abhishek360
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import * as Colors from '../configs/colors';
class About extends Component{

  render(){
    return (
      <div
        style = {styles.container}
      >
        <h2> Happy to help!</h2>
      </div>
    );
  }
}

const styles = {
  container: {
    position: 'absolute',
    width: '100%',
    margin: 10,
    left: '50%',
    top: '50%',
    transform: 'translate(-50%, -50%)'
  },
};



export default About;
