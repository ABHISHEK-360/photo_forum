//abhishek360

import React from 'react';
import {
  artsEvents,
} from '../configs/content';
import {
  Grid,
  Typography,
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  CardActions,
  Button
} from '@material-ui/core'
import * as Colors from '../configs/colors';

class Arts extends React.Component {
  render() {
    return (
      <div style = { styles.container }>
        <Grid
          container
          direction = 'row'
          justify = 'space-evenly'
          alignItems = 'center'
          style = {{ height: '90%'}}
        >
          {
            artsEvents.map(item => {
              return (
                <Grid
                  item lg = {3}
                  key = { item.title }
                >
                <Card style = {{ margin: 25,}}>
                  <CardActionArea style = { styles.actionAreaCard }>
                    <CardMedia
                      component = "img"
                      alt = "asdfghj"
                      style = {{ height: '100%', width: '100%' }}
                      image = { item.img }
                      tittle = 'Photography'
                    />
                    <CardContent>
                      <Typography
                        gutterBottom
                        variant = 'h4'
                        component = 'h2'
                      >
                        { item.title }
                      </Typography>
                      <Typography
                        gutterBottom
                        variant = 'h6'
                        component = 'h2'
                      >
                        { item.desc }
                      </Typography>
                    </CardContent>
                  </CardActionArea>
                  <CardActions style = {styles.cardAction}>
                    <Button
                      size = 'small'
                      color = 'primary'
                    >
                      Register
                    </Button>
                  </CardActions>
                </Card>
              </Grid>
              )
            })
          }
        </Grid>
      </div>
    );
  }
}

const styles = {
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  cardAction: {
    backgroundColor: Colors.AQUAMARINE,
  },
  button: {
    margin: 10,
    background: 'purple',
  },
};


export default Arts;
