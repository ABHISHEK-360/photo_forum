//abhishek360

import React from 'react';
import * as Colors from '../configs/colors';

class Photography extends React.Component {
  render() {
    return (
      <div style = { styles.container }>
        <h3 style = {{ color: Colors.MAASTRICHT_BLUE, margin: 10}} align = 'right'> Developed By: Abhishek360 </h3>
      </div>
    );
  }
}

const styles = {
  container: {
    position: 'static',
    height: 30,
    backgroundColor: Colors.QUICK_SILVER,
    bottom: 0,
  },
};

export default Photography;
