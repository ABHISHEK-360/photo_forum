//abhishek360

import React, { Component } from 'react';
import { AppBar, Toolbar } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import AuthHOC from '../HOC/AuthHOC';
import { connect } from 'react-redux';
import NavBar from './NavBar.js'
import * as Colors from '../configs/colors'

class Header extends Component{

  render(){
    return(
      <div style = { styles.container } >
        <AppBar
          style = { styles.appBarContainer }
          position = 'static'
        >
          <Toolbar>
          <Typography
            variant = 'caption'
            style = {styles.nameText }
          >
            OurForum
          </Typography>
          <div style = {{flex: 5}}>
            <NavBar/>
          </div>
          <AuthHOC
            yes = {() =>
              <div align = 'center' style = {{ flex: 3 }}>
                  {
                    this.props.userDetails.email &&
                      <Typography
                        variant = 'caption'
                      >
                        Hello, { this.props.userDetails.email }
                      </Typography>
                  }
                  <Button
                  id = 'headerLogoutButton'
                  style={styles.button}
                  onClick={(event) => this.props.handleLogout()}
                >
                  Logout
                </Button>
              </div>

            }
            no = {() =>
              <div align = 'center' style = {{ flex: 3 }}>
                <Button
                  id = 'headerLoginButton'
                  style={styles.button}
                  onClick={(event) => this.props.togglePopup('login')}
                >
                  Login
                </Button>
                <Button
                  id = 'headerRegisterButton'
                  style={styles.button}
                  onClick={(event) => this.props.togglePopup('signup')}
                >
                  Register
                </Button>
              </div>
            }
          />
          </Toolbar>
        </AppBar>
      </div>
    )
  }
}

const styles = {
  container: {
    width: '100%',
  },
  appBarContainer: {
    width: '100%',
    color: '#A4A7B2',
    display: 'flex',
    backgroundColor: Colors.MAASTRICHT_BLUE
  },
  nameText: {
    fontSize: 25,
    flex: 2,
    color: Colors.LAPIS_LAZULI

  },
  textField: {
    width: '20',
    margin: 10,
  },
  button: {
    marginLeft: 10,
    marginRight: 10,
    width: '20',
    color: Colors.QUICK_SILVER,
    background: Colors.IMPERIAL,
  },
};

const mapStateToProps = ({ userDetails }) => ({ userDetails });

export default connect(mapStateToProps, {

  })(Header);
