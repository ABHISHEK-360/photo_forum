import React, { Component } from 'react';
import * as Colors from '../configs/colors';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

class Register extends Component {
  state= {
    first_name: '',
    last_name: '',
    phone: '',
    email: '',
    password: '',
    confPassword: '',
  }

  verifyRegister = () => {
    const { first_name, last_name, phone, password, confPassword, email } = this.state;
    if(first_name===''){
      alert('First Name is Required!');
      return;
    }
    else if(email===''){
      alert('Email is Required!');
      return;
    }
    else if(email.search('@')<0){
      alert('Enter a valid Email!');
      return;
    }
    else if(phone===''){
      alert('Phone is Required!');
      return;
    }
    else if(password===''){
      alert('Password is Required!');
      return;
    }
    else if(password.length<8){
      alert('Password is Too Short!');
      return;
    }
    else if(password!==confPassword){
      alert('Password Mismatch!');
      return;
    }
    else{
      this.props.handleRegister({
        firstName: first_name,
        lastName: last_name,
        email: email,
        phone: phone,
        password: password
      });
    }

  }

  render() {
    return (
      <div
      >
        <h1 align = "center"> Create an Account </h1>
        <h4 align = "center"> Be a part of OurForum Community. </h4>
        <div
          align = 'center'
          style = { styles.container }
        >
        <TextField
          hintText="Enter your First Name"
          placeholder = "First Name"
          style = { styles.textField }
          onChange = {(event) => this.setState({first_name:event.target.value})}
        />
        <br/>
        <TextField
          hintText="Enter your Last Name"
          placeholder ="Last Name"
          style = { styles.textField }
          onChange = {(event) => this.setState({last_name:event.target.value})}
        />
        <br/>
        <TextField
          hintText = "Enter your Email"
          placeholder = "Email"
          style = { styles.textField }
          onChange = {(event) => this.setState({email:event.target.value})}
        />
        <br/>
        <TextField
          hintText="Enter your Phone"
          placeholder="Phone"
          style = { styles.textField }
          onChange = {(event) => this.setState({phone:event.target.value})}
        />
        <br/>
        <TextField
          type = "password"
          hintText="Enter your Password"
          placeholder="Password"
          style = { styles.textField }
          onChange = {(event) => this.setState({password:event.target.value})}
        />
        <br/>
        <TextField
          type = "password"
          hintText="Enter the Password Provided"
          placeholder="Confirm Password"
          style = { styles.textField }
          onChange = {(event) => this.setState({confPassword:event.target.value})}
        />
        <br/>
        <div
          align = 'center'
        >
        <Button
          color = 'default'
          style={styles.button}
          onClick={(event) => this.verifyRegister()}
        >
          Register
        </Button>
        </div>
        </div>
        <div
          style = { styles.footer }
        >
          <div
            align = 'center'
          >
          Already Registered?
          <Button
            style={styles.footerButton}
            onClick={(event) => this.props.changePopupType('login')}
          >
            Login
          </Button>
          </div>
        </div>
      </div>
    );
  }
}

const styles = {
  container: {
    position: 'absolute',
    margin: 15,
    width: '100%',
    left: '50%',
    top: '50%',
    transform: 'translate(-50%, -50%)'
  },
  textField: {
    width: '75%',
    marginTop: 20,
  },
  button: {
    margin: 10,
    width: '75%',
    color: Colors.QUICK_SILVER,
    background: Colors.PURPLE,
  },
  footer: {
    position: 'absolute',
    bottom: '0%',
    width: '100%',
    borderRadius: 5,
    background: Colors.AQUAMARINE,
  },
  footerButton: {
    margin: 10,
    color: Colors.QUICK_SILVER,
    background: 'purple',
  },
};

export default Register;
