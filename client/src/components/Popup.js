import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import { Close  } from '@material-ui/icons';
import Login from './Login';
import Register from './Register';
import './style.css';

class Popup extends React.Component {
  render() {
    return (
      <div className = 'popup'>
        <div className = 'popup_inner'>
        <div className = 'popup_close'>
          <IconButton
            onClick={this.props.closePopup}
          >
            <Close/>
          </IconButton>
        </div>
        {
          (this.props.type === 'login') &&
          <Login
            handleLogin = { this.props.handleLogin }
            changePopupType = { this.props.changePopupType }
          />
        }
        {
          (this.props.type === 'signup') &&
          <Register
            handleRegister = { this.props.handleRegister }
            changePopupType = { this.props.changePopupType }
          />
        }
        </div>
      </div>
    );
  }
}

const styles = {
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    margin: 10,
  },
  button: {
    margin: 10,
    background: 'purple',
  },
};


export default Popup;
