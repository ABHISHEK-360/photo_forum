export const QUICK_SILVER = '#A4A7B2';
export const MAASTRICHT_BLUE = '#011936';
export const LAPIS_LAZULI = '#2274A5';
export const IMPERIAL = '#643173';
export const AQUAMARINE = '#5DFDCB';
export const PURPLE = 'purple';
export const LIGHT_CRIMSON = '#EA638C';
