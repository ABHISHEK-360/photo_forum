export const category = [
  {
    key: 1,
    value: 'photography',
    title: 'Photography',
    img: 'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260',
    desc: 'World of Photography!'
  },
  {
    key: 2,
    value: 'architecture',
    title: 'Architecture',
    img: 'https://images.pexels.com/photos/227675/pexels-photo-227675.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    desc: 'World of Architecture!'
  },
  {
    key: 3,
    value: 'art',
    title: 'Art',
    img: 'https://images.pexels.com/photos/990824/pexels-photo-990824.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    desc: 'World of Art!'
  },
]

export const photoEvents = [
  {
    title: 'photo event 1',
    img: 'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260',
    desc: ' photo event 1 desc'
  },
  {
    title: 'photo event 2',
    img: 'https://images.pexels.com/photos/227675/pexels-photo-227675.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    desc: 'photo event 2 desc'
  },
  {
    title: 'photo event 3',
    img: 'https://images.pexels.com/photos/990824/pexels-photo-990824.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    desc: 'photo event 3 desc'
  },
  {
    title: 'photo event 4',
    img: 'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260',
    desc: ' photo event 1 desc'
  },
  {
    title: 'photo event 5',
    img: 'https://images.pexels.com/photos/227675/pexels-photo-227675.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    desc: 'photo event 2 desc'
  },
  {
    title: 'photo event 6',
    img: 'https://images.pexels.com/photos/990824/pexels-photo-990824.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    desc: 'photo event 3 desc'
  },
]

export const photoSlides = [
  {
    title: 'photo slide 1',
    img: 'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260',
  },
  {
    title: 'photo slide 2',
    img: 'https://images.pexels.com/photos/227675/pexels-photo-227675.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
  },
  {
    title: 'photo slide 3',
    img: 'https://images.pexels.com/photos/990824/pexels-photo-990824.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
  },
]

export const archiEvents = [
  {
    title: 'archi event 1',
    img: 'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260',
    desc: ' archi event 1 desc'
  },
  {
    title: 'archi event 2',
    img: 'https://images.pexels.com/photos/227675/pexels-photo-227675.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    desc: 'archi event 2 desc'
  },
  {
    title: 'archi event 3',
    img: 'https://images.pexels.com/photos/990824/pexels-photo-990824.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    desc: 'archi event 3 desc'
  },
  {
    title: 'archi event 4',
    img: 'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260',
    desc: ' archi event 1 desc'
  },
  {
    title: 'archi event 5',
    img: 'https://images.pexels.com/photos/227675/pexels-photo-227675.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    desc: 'archi event 2 desc'
  },
  {
    title: 'archi event 6',
    img: 'https://images.pexels.com/photos/990824/pexels-photo-990824.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    desc: 'archi event 3 desc'
  },
]

export const artsEvents = [
  {
    title: 'arts event 1',
    img: 'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260',
    desc: ' arts event 1 desc'
  },
  {
    title: 'arts event 2',
    img: 'https://images.pexels.com/photos/227675/pexels-photo-227675.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    desc: 'arts event 2 desc'
  },
  {
    title: 'arts event 3',
    img: 'https://images.pexels.com/photos/990824/pexels-photo-990824.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    desc: 'arts event 3 desc'
  },
  {
    title: 'arts event 4',
    img: 'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260',
    desc: ' arts event 1 desc'
  },
  {
    title: 'arts event 5',
    img: 'https://images.pexels.com/photos/227675/pexels-photo-227675.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    desc: 'arts event 2 desc'
  },
  {
    title: 'arts event 6',
    img: 'https://images.pexels.com/photos/990824/pexels-photo-990824.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    desc: 'arts event 3 desc'
  },
]
