//abhishek360
import { combineReducers } from 'redux';

import UserDetailsReducer from './reducers/UserDetailsReducer';



const appReducer = combineReducers({
  userDetails: UserDetailsReducer,
  
});

const rootReducer = (state, action) => {
  if (action.type === 'USER_LOGOUT') {
    state = undefined
  }

  return appReducer(state, action)
}

export default rootReducer;
